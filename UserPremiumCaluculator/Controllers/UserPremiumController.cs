﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UserPremiumCaluculator.Models;
using System.Threading.Tasks;

namespace UserPremiumCaluculator.Controllers
{
    public class UserPremiumController : Controller
    {
        // GET: UserPremium
        public ActionResult Index()
        {

            return View("UserPremiumView");
        }
        [HttpPost]
        [AllowAnonymous]
        public JsonResult CalculatePremium(string Name, string birthdate, string Gender)
        {
            try
            {
                string[] result = new string[2];
                DateTime DOB = DateTime.ParseExact(birthdate, "dd/MM/yyyy", null);
                var calculatedage = Getage(DOB);
                if (calculatedage >= Constants.AGE_EIGHTEEN && calculatedage <= Constants.AGE_SIXTYFIVE)
                {
                    double Genderfactor;
                    if (Gender == "Male")
                    {
                        Genderfactor = Constants.GENDERFACTOR_MALE;
                    }
                    else
                    {
                        Genderfactor = Constants.GENDERFACTOR_FEMALE;
                    }

                    var Premium = calculatedage * Genderfactor * Constants.Hundred;
                    var roundedPremium = Math.Round(Premium, 2);
                    result[0] = Name;
                    result[1] = Convert.ToString(roundedPremium);
                }
                else
                {
                    result[0] = Name;
                    result[1] = "Not available";
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public double Getage(DateTime dob)
        {
            DateTime now = DateTime.Today;
            int age = now.Year - dob.Year;
            return age;
        }

    }
}