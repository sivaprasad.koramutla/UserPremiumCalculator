﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(UserPremiumCaluculator.Startup))]
namespace UserPremiumCaluculator
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
