﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace UserPremiumCaluculator.Models
{
    public class UserPremiumModel
    {
        public string Name { get; set; }
        public DateTime DateofBirth { get; set; }
        public string Gender { get; set; }
        public double Premium { get; set; }
        public bool IsFemale { get; set; }
    }
}