﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserPremiumCaluculator
{
    public static class Constants
    {
        public const double GENDERFACTOR_MALE = 1.2;
        public const double GENDERFACTOR_FEMALE = 1.1;
        public const int Hundred = 100;
        public const int AGE_EIGHTEEN = 18;
        public const int AGE_SIXTYFIVE = 65;
    }
}